<?php
namespace App\Services;

use GuzzleHttp\Client;

class ValidationService
{
    const API_COUNTRY_CODES = 'https://experum.ru/te/country-codes';
    const API_TIMEZONES = 'https://experum.ru/te/timezones';

    public function __construct()
    {
        $this->guzzle = new Client;
    }

    /**
     * validate country code
     * @param  String $code Country code
     * @return boolean valid\invalid
     */
    public function validateCountryCode(String $code)
    {
        $countryCodes = $this->getCountryCodes();
        return isset($countryCodes[$code]);
    }

    /**
     * Get country codes from API
     * @return array Country codes
     */
    protected function getCountryCodes(int $attempts = 0)
    {
        try {
            $body = $this->guzzle->get(self::API_COUNTRY_CODES)->getBody()->getContents();
        } catch (\Throwable $e) {
            if ($attempts < 3) {
                usleep(500000);
                return $this->getCountryCodes($attempts+1);
            }
        }
        $data = json_decode($body, true);

        return $data;
    }

    /**
    * Validate timezone
    * @param  String $code Cody country
    * @return boolean valid\invalid
    */
    public function validateTimezone(String $timezone)
    {
        $timezones = $this->getTimezones();
        return isset($timezones[$timezone]);
    }

    /**
     * Get timezones from API
     * @return array Timezones
     */
    protected function getTimezones()
    {
        try {
            $body = $this->guzzle->get(self::API_TIMEZONES)->getBody()->getContents();
        } catch (\Throwable $e) {
            if ($attempts < 3) {
                usleep(500000);
                return $this->getCountryCodes($attempts+1);
            }
        }
        $data = json_decode($body, true);

        return $data;
    }
}
