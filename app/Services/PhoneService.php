<?php
namespace App\Services;

use App\Phone;

class PhoneService
{
    /**
     * Find phones by first_name or last_name
     * @param  String|string $search Search query
     * @return array Phones
     */
    public function searchPhones(String $search = '')
    {
        if (empty($search)) {
            $phones = Phone::all();
        } else {
            $phones = Phone::where('first_name', 'LIKE', '%'.$search.'%')->orWhere('last_name', 'LIKE', '%'.$search.'%')->get();
        }

        return $phones->toArray();
    }

    /**
     * Update phone by id
     * @param  int    $phoneId Phone ID
     * @param  array  $data    Phone data
     * @return App\Phone updated phone
     */
    public function updatePhone(int $phoneId, array $data)
    {
        $phone = Phone::findOrFail($phoneId);
        $phone->update($data);

        return $phone;
    }

    /**
     * Delete phone by id
     * @param  int    $phoneId phone ID
     * @return boolean
     */
    public function deletePhone(int $phoneId)
    {
        $phone = Phone::findOrFail($phoneId);

        $phone->delete();

        return true;
    }

    /**
     * Create new phone
     * @param  array  $data Phone data
     * @return App\Phone created phone
     */
    public function createPhone(array $data)
    {
        $phone = Phone::create($data);

        return $phone;
    }
}
