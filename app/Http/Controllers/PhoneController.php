<?php

namespace App\Http\Controllers;

use App\Http\Requests\PhoneRequest;
use App\Services\PhoneService;
use App\Services\ValidationService;
use Illuminate\Http\Request;

class PhoneController extends Controller
{
    public function __construct()
    {
        $this->validationService = new ValidationService;
        $this->phoneService = new PhoneService;
    }

    /**
     * Get all phones or find by first_name and last_name
     * @param  Request $request Request with search query-
     * @return array phones
     */
    public function index(Request $request)
    {
        $searchQuery = $request->get('search', '');
        $phones = $this->phoneService->searchPhones($searchQuery);

        return $phones;
    }

    /**
     * Delete phone
     * @param  int    $phoneId Phone ID
     * @return \Illuminate\Auth\Access\Response
     */
    public function destroy(int $phoneId)
    {
        $status = $this->phoneService->deletePhone($phoneId);

        return response()->json(['success' => $status]);
    }

    /**
     * Delete phone
     * @param  int    $phoneId Phone ID
     * @param  PhoneRequest $request Request with phone data
     * @return \Illuminate\Auth\Access\Response
     */
    public function update(int $phoneId, PhoneRequest $request)
    {
        $data = $request->validated();

        if (!$this->validationService->validateCountryCode($data['country_code'])) {
            return response()->json(['message' => 'country_code is invalid.'], 422);
        }
        if (!$this->validationService->validateTimezone($data['timezone'])) {
            return response()->json(['message' => 'timezone is invalid.'], 422);
        }
        $phone = $this->phoneService->updatePhone($phoneId, $data);

        return $phone;
    }

    /**
     * Create new phone
     * @param  PhoneRequest $request Request with phone data
     * @return App\Phone Craeted Phone
     */
    public function store(PhoneRequest $request)
    {
        $data = $request->validated();

        if (!$this->validationService->validateCountryCode($data['country_code'])) {
            return response()->json(['message' => 'country_code is invalid.'], 422);
        }
        if (!$this->validationService->validateTimezone($data['timezone'])) {
            return response()->json(['message' => 'timezone is invalid.'], 422);
        }

        $phone = $this->phoneService->createPhone($data);

        return $phone;
    }
}
